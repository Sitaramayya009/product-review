package com.epam.microservices.productreview.service;

import java.util.List;

import com.epam.microservices.productreview.model.ProductReview;

public interface ProductReviewService {

	public List<ProductReview> getProductReviews(Long id);

	public ProductReview saveProductReviews(ProductReview review, Long prodId);

	public boolean deleteProductReview(Long prodId, Long reviewId);

	public ProductReview updateProductReview(ProductReview review, Long reviewId, Long prodId);

}
