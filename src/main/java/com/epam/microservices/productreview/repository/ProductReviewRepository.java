package com.epam.microservices.productreview.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.microservices.productreview.model.ProductReview;

@Repository
public interface ProductReviewRepository extends JpaRepository<ProductReview, Long> {

	List<ProductReview> findByProdId(Long prodId);

	ProductReview findByIdAndProdId(Long reviewId, Long prodId);

	ProductReview deleteByIdAndProdId(Long reviewId, Long prodId);

}
