package com.epam.microservices.productreview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.epam.microservices.productreview.model.ProductReview;
import com.epam.microservices.productreview.service.ProductReviewService;

@RestController
public class ProductReviewController {

	@Autowired
	private ProductReviewService productReviewService;

	/**
	 * @param secret
	 * @param prodId
	 * @return
	 */
	@GetMapping("/products/{prodId}/reviews")
	// @HystrixCommand(fallbackMethod = "fallbackGetProductReviews")
	public ResponseEntity<List<ProductReview>> getProductReviews(@RequestHeader(value = "SharedSecret") String secret,
			@PathVariable Long prodId) {
		return ResponseEntity.ok(productReviewService.getProductReviews(prodId));
	}

	public ResponseEntity<List<ProductReview>> fallbackGetProductReviews() {
		List<ProductReview> reviewList = new ArrayList<>();
		reviewList.add(new ProductReview(1, 1, 3, "good to go..."));
		return ResponseEntity.ok(reviewList);
	}

	/**
	 * 
	 * @param secret
	 * @param review
	 * @param prodId
	 * @return
	 */
	@PostMapping("/products/{prodId}/reviews")
	public ResponseEntity<ProductReview> saveProductReviews(@RequestHeader(value = "API-KEY") String secret,
			@RequestBody ProductReview review, @PathVariable Long prodId) {

		return ResponseEntity.ok(productReviewService.saveProductReviews(review, prodId));
	}

	/**
	 * 
	 * @param prodId
	 * @param reviewId
	 * @return
	 */
	@DeleteMapping("/products/{prodId}/reviews/{reviewId}")
	public ResponseEntity<Boolean> deleteProductReviews(@PathVariable Long prodId, @PathVariable Long reviewId) {
		return ResponseEntity.ok(productReviewService.deleteProductReview(reviewId, prodId));
	}

	/**
	 * 
	 * @param review
	 * @param reviewId
	 * @param prodId
	 * @return
	 */

	@PutMapping("/products/{prodId}/reviews/{reviewId}")
	public ResponseEntity<ProductReview> updateProductReviews(@RequestBody ProductReview review,
			@PathVariable Long reviewId, @PathVariable Long prodId) {
		ProductReview updateReview = productReviewService.updateProductReview(review, reviewId, prodId);
		return ResponseEntity.ok(updateReview);
	}

}
